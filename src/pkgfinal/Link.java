package pkgfinal;
//Exercise 1-a -> Create Linked Lists
public class Link {
        public String name;
        public String surname;
        public int age;
        public int bank[];
        public Link next;
        
        Link(String Name,String Surname, int Age, int Bank[]){
        this.name=Name;
        this.surname = Surname;
        this.age = Age;
        this.bank = Bank;
    }
    
    public void display(){
        System.out.println("Name: "+ this.name);
    }
    
    public String get(){
        return this.name;
    }
}

class LinkedList{
    public Link  first;
    
    public void LinkedLists(){
        this.first = null;
    }
    
    public boolean isEmpty(){
        return (this.first == null);
    }
    
    public void insertFirst(String name, String surname, int age, int bank[]){
        Link newLink = new Link(name, surname, age, bank);
        newLink.next = first;
        first = newLink;
    }
    
    public void insertEnd(String name, String surname, int age, int bank[]){
        Link newLink = new Link(name, surname, age, bank);
        if(isEmpty()){
            first = newLink;
            return;
        }
        
        Link q = first;
        while(q.next !=null){
            q = q.next;
        }
        q.next = newLink;
    }
    
    public Link deleteFirst(){
        if(first == null){
            System.out.println("Empty!!!!");
        }
            Link tmp = first;
            first = first.next;
            return tmp;
    }
    
    public void displayList(){
        if(isEmpty()){
            System.out.println("Empty!!!");
            return;
        }
        Link tmp = first;
        while(tmp != null){
            System.out.println("+-------------------------------------------+");
            System.out.println("Name: "+tmp.name);
            System.out.println("Surname: "+tmp.surname);
            System.out.println("Age: "+tmp.age);
            for(int i=0;i<tmp.bank.length;i++){
                System.out.println("Bank account "+i+": "+tmp.bank[i]);
            }
            tmp = tmp.next;
        }
        System.out.println("");
    }
    //Exercise 1-b -> Sort the linked lists
    public void Sort(){
        if(isEmpty()){
            System.out.println("+---------------------+");
            System.out.println("Please initialize the List first.");
            System.out.println("+---------------------+");
            return;
        }
        Link q=first;
        while(q!=null){
            Link t=q;
            while(t!=null){
                if(q.age<t.age){
                    int tmp=q.age;
                    q.age=t.age;
                    t.age=tmp;
                }
                t=t.next;
            }
            q=q.next;
        }
        System.out.println("+---------------------+");
        System.out.println("Linked List has been sorted successfully.");
    }
    
    //eExercise 1-c -> Find the maximum by total of the bank accounts
    public void MaxbyBA(){
        if(isEmpty()){
            System.out.println("+---------------------+");
            System.out.println("Please initialize the List first.");
            System.out.println("+---------------------+");
            return;
        }
        Link q=first;
        String name = "";
        String surname = "";
        int age = 0;
        int ba = 0;
        while(q!=null){
            if(q.bank.length>ba){
                name=q.name;
                surname=q.surname;
                age=q.age;
                ba=q.bank.length;
            }
            q=q.next;
        }
        System.out.println(name+" "+surname+" has "+ba+" bank accounts and he/she is "+age+" years old.");
    }
    
    //Exercise 1-d -> Find the younges;
    public void FindYoungest(){
        if(isEmpty()){
            System.out.println("+---------------------+");
            System.out.println("Please initialize the List first.");
            System.out.println("+---------------------+");
            return;
        }
        Link q=first;
        String name = q.name;
        String surname = q.surname;
        int youngest=q.age;
        while(q!=null){
            if(q.age<youngest){
                youngest=q.age;
                name = q.name;
                surname = q.surname;
            }
            q=q.next;
        }
        System.out.println("+---------------------+");
        System.out.println("Youngest is: "+name+" "+surname+", and he/she is "+youngest+" years old.");
        System.out.println("+----------------------------------------+");
    }
    
    //Exercise 1-e -> Remove an element by name
    public void RemoveElement(String name){
        while(first!=null && first.name.equals(name))
            first=first.next;
        if(first==null)
            return;
        /*first method
        Link q=first;
        while(q!=null && q.next!=null){
            if(q.next.data==value){
                Link t=q.next;
                while(t!=null && t.data==value)
                    t=t.next;
                q.next=t;                
            }
            q=q.next;
        }
        */
        //second method
        Link q=first;
        while(q.next!=null){
            if(q.next.name.equals(name))
                q.next=q.next.next;
            else
                q=q.next;
        }
    }
    
    public void ChangeBA(int val, String name, String surname, int bank){
        if(isEmpty()){
            System.out.println("+---------------------+");
            System.out.println("Please initialize the List first.");
            System.out.println("+---------------------+");
            return;
        }
        Link q=first;
        while(q!=null){
            if(q.name==name && q.surname==surname && q.bank.length>=bank){
                int tmp=q.bank[bank-1];
                q.bank[bank-1]=val;
                System.out.println("Vale "+tmp+" has been changed to "+val);
                return;
            }
        }
        System.out.println("Bank account not found.");
    }
    
    public boolean existinList(String val){
        String tmp=val;
        Link q=first;
        while(q!=null){
            if(q.name.equals(tmp)){
                return true;
            }
            q=q.next;
        }
        return false;
    }
    
    
}
