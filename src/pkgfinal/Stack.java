package pkgfinal;
public class Stack {
    private int maxSize;
    private String names[];
    private String surnames[];
    private int ages[];
    private int bankAccounts[];
    private int n;
    public Stack(int num){
        maxSize=num;
        names = new String[maxSize];
        surnames = new String[maxSize];
        ages = new int[maxSize];
        bankAccounts = new int[maxSize];
        int n=0;
    }
    public boolean isEmpty(){
        return n==0;
    }
    public boolean isFull(){
        return maxSize==n;
    }
    public int size(){
        return n;
    }
    public int freeSpace(){
        return maxSize-n;
    }
    public void push(String name, String surname, int age, int bankaccount){
        names[n] = name;
        surnames[n] = surname;
        ages[n] = age;
        bankAccounts[n] = bankaccount;
        n++;       
    }
    public void pop(){
        n--;
    }
    public void showStack(){
        System.out.println("+--------------------------------+");
        for(int i=0; i<n;i++){
            System.out.println("Name: "+names[i]);
            System.out.println("Surname: "+surnames[i]);
            System.out.println("Age: "+ages[i]);
            System.out.println("Total bank accounts: "+ bankAccounts[i]);
            System.out.println("+--------------------------------+");
        }
    }
    public Stack copyStack(Stack lm){
        return lm;
    }
    public int peekAge(){
        if(isEmpty()){
            return -999999;
        }
        return ages[n];
    }
    public int peekBA(){
        if(isEmpty()){
            return -999999;
        }
        return bankAccounts[n];
    }
    public void topElement(){
        System.out.println("+--------------------------------+");
        System.out.println("Top element is: "+names[size()-1]+" and he/she has "+bankAccounts[size()-1]+" bank accounts.");
        System.out.println("+--------------------------------+");
    }
}
