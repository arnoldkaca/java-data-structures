package pkgfinal;
import java.util.Scanner;
public class Console{
    public static void main(String[] args) {
        Start();
        
    }
    static void Start(){
        String[] select = {"Arrays", "Structures", "Linked Lists", "Queues", "Stacks", "Trees"};
        Scanner scan = new Scanner(System.in);
       while(true){
            System.out.println("+--------------------------------+");
            for(int i=0;i<select.length;i++){
                System.out.println((i+1) + ". "+select[i]+".");
            }
            System.out.println("+--------------------------------+");
            System.out.println("Please select one from the list above...");
            System.out.println("+--------------------------------+");
            int number = getScan(scan, 1, select.length);
            
            if(number == 1){
                System.out.println("");
                System.out.println("+--------------------------------+");
                System.out.println("1. Arrays have been selected...");
                System.out.println("+--------------------------------+");
                System.out.println("");
                ArrayFunc(scan);
            }
            if(number == 2){
                System.out.println("");
                System.out.println("+--------------------------------+");
                System.out.println("2. Structures have been selected...");
                System.out.println("+--------------------------------+");
                System.out.println("");
                StructFunc(scan);
            }
            if(number == 3){
                System.out.println("");
                System.out.println("+--------------------------------+");
                System.out.println("3. Linked Lists have been selected...");
                System.out.println("+--------------------------------+");
                System.out.println("");
                LinkedFunc(scan);
            }
            if(number == 4){
                System.out.println("");
                System.out.println("+--------------------------------+");
                System.out.println("4. Queues have been selected...");
                System.out.println("+--------------------------------+");
                System.out.println("");
                QueueFunc(scan);
            }
            if(number == 5){
                System.out.println("");
                System.out.println("+--------------------------------+");
                System.out.println("5. Stacks have been selected...");
                System.out.println("+--------------------------------+");
                System.out.println("");
                StackFunc(scan);
            }
            if(number == 6){
                System.out.println("");
                System.out.println("+--------------------------------+");
                System.out.println("6. Trees have been selected...");
                System.out.println("+--------------------------------+");
                System.out.println("");
                BinTreeFunc(scan);
            }
       }
    }
    static void ArrayFunc(Scanner scan){
        //Arrays Functions here:
            System.out.println("+--------------------------------+");
            System.out.println("1. Define the size of the array");
            System.out.println("2. Sort the array.");
            System.out.println("3. Find the maximum in the array.");
            System.out.println("4. Find the mode in the array.");
            System.out.println("5. Does the array have an equillibrium?");
            System.out.println("6. Remove an element from the array.");
            System.out.println("7. Add an element.");
            System.out.println("8. Show Array.");
            System.out.println("0. Go Back");
            System.out.println("+--------------------------------+");
            while(true){
                int scanned = getScan(scan, 0, 8);
                if(scanned==0){
                    Start();
                }
                if(scanned==1){
                    System.out.println("+---------------------+");
                    System.out.println("Please write the array size.");
                    System.out.println("+---------------------+");
                    int arrsize = getScan(scan, 0, 9999999);
                    Arrays.setArraySize(arrsize);
                    System.out.println("+---------------------+");
                    System.out.println("Array size has been set to "+arrsize);
                    System.out.println("+---------------------+");
                    System.out.println("");
                    System.out.println("Please insert array elements...");
                    System.out.println("+---------------------+");
                    for(int i=0;i<arrsize;i++){
                        Arrays.array[i]=getScan(scan, -9999999, 9999999);
                    }
                    Arrays.arrayset=true;
                    System.out.println("Values Added to the array successfully.");
                    System.out.println("+---------------------+");
                    ArrayFunc(scan);
                }
                if(scanned==2){
                    if(Arrays.arrayset){
                        Arrays.SortArray(Arrays.array);
                        System.out.println("+---------------------+");
                        System.out.println("Array has been sorted successfully.");
                        System.out.println("+---------------------+");
                    } else {
                        System.out.println("+---------------------+");
                        System.out.println("Please initialize array first.");
                        System.out.println("+-------------------------+");
                    }
                }
                if(scanned==3){
                    if(Arrays.arrayset){
                        System.out.println("+---------------------+");
                        System.out.println("Maximum in the array is: "+Arrays.FindMax(Arrays.array));
                        System.out.println("+---------------------+");
                    } else {
                        System.out.println("+---------------------+");
                        System.out.println("Please initialize array first.");
                        System.out.println("+-------------------------+");
                    }
                }
                if(scanned==4){
                    if(Arrays.arrayset){
                        System.out.println("+---------------------+");
                        System.out.println("Mode in the array is: "+Arrays.FindMode(Arrays.array));
                        System.out.println("+---------------------+");
                    } else {
                        System.out.println("+---------------------+");
                        System.out.println("Please initialize array first.");
                        System.out.println("+-------------------------+");
                    }
                }
                if(scanned==5){
                    if(Arrays.arrayset){
                        System.out.println("+---------------------+");
                        if(Arrays.HasEquillibriumElement(Arrays.array)){
                            System.out.println("Yes, this array has an equillibrium element.");
                            System.out.println("+---------------------+");
                        }
                        else {System.out.println("No, this array doesn't have an equillibrium element.");}
                        System.out.println("+---------------------+");
                    } else {
                        System.out.println("+---------------------+");
                        System.out.println("Please initialize array first.");
                        System.out.println("+-------------------------+");
                    }
                }
                if(scanned==6){
                    if(Arrays.arrayset){
                        System.out.println("+---------------------+");
                        System.out.println("Please insert the element you want to delete...");
                        System.out.println("+---------------------+");
                        int rem = getScan(scan, -999999, 9999999);
                        if(Arrays.ExistsinArray(Arrays.array, rem)){
                            Arrays.RemoveElement(Arrays.array, rem);
                            System.out.println("Element "+rem+" has successfully been removed from array.");
                            System.out.println("+---------------------+");
                        }
                        else {System.out.println("Element "+rem+" does not exist in the array.");}
                        System.out.println("+---------------------+");
                    } else {
                        System.out.println("+---------------------+");
                        System.out.println("Please initialize array first.");
                        System.out.println("+-------------------------+");
                    }
                }
                if(scanned==7){
                    if(Arrays.arrayset){
                        System.out.println("+---------------------+");
                        System.out.println("Please insert it's position...");
                        System.out.println("+---------------------+");
                        int pos = getScan(scan, -999999, 9999999);
                        System.out.println("Please insert the value...");
                        System.out.println("+---------------------+");
                        int val = getScan(scan, -999999, 9999999);
                        Arrays.AddElement(Arrays.array, pos, val);
                        System.out.println(val+" has successfully been added at "+pos+" position.");
                        System.out.println("+---------------------+");
                    } else {
                        System.out.println("+---------------------+");
                        System.out.println("Please initialize array first.");
                        System.out.println("+-------------------------+");
                    }
                }
                if(scanned==8){
                    if(Arrays.arrayset){
                        System.out.println("+---------------------+");
                        Arrays.ShowArray(Arrays.array);
                        System.out.println("+---------------------+");
                        System.out.println("");
                    } else {
                        System.out.println("+---------------------+");
                        System.out.println("Please initialize array first.");
                        System.out.println("+-------------------------+");
                    }
                }
            }
    }
    
    static void LinkedFunc(Scanner scan){
        System.out.println("+--------------------------------+");
        System.out.println("1. Create a Linked List.");
        System.out.println("2. Sort the Linked List by age.");
        System.out.println("3. Find the maximum by total of bank accounts.");
        System.out.println("4. Find the youngest.");
        System.out.println("5. Remove an element.");
        System.out.println("6. Add an element in the begining.");
        System.out.println("7. Modify bank account");
        System.out.println("8. Show Linked List.");
        System.out.println("0. Go Back");
        System.out.println("+--------------------------------+");
        LinkedList lm = new LinkedList();
        while(true){
                int scanned = getScan(scan, 0, 8);
                if(scanned==0){
                    Start();
                }
                if(scanned==1){
                    System.out.println("How many persons you want to register?");
                    int tp=getScan(scan, -999999, 999999);
                    for(int i=0;i<tp;i++){
                        System.out.println("Please insert the name.");
                        String name = getsScan(scan);
                        System.out.println("Please insert the surname.");
                        String surname = getsScan(scan);
                        System.out.println("Please insert the age.");
                        int age = getScan(scan, 0, 150);
                        System.out.println("Please insert the total of bank accounts.");
                        int totalbankaccounts = getScan(scan, 0, 200);
                        int[] banks = new int[totalbankaccounts];
                        for(int j=0;j<totalbankaccounts;j++){
                            System.out.println("Insert bank account nr. "+(j+1)+" of "+totalbankaccounts);
                            int bnt=getScan(scan, -99, 999999);
                            banks[j]=bnt;
                        }
                        lm.insertEnd(name, surname, age, banks);
                        System.out.println(name+" "+surname+" has been added successfully.");
                    }
                    System.out.println("+------------------------------+");
                }
                if(scanned==2){
                    lm.Sort();
                }
                if(scanned==3){
                    lm.MaxbyBA();
                }
                if(scanned==4){
                    lm.FindYoungest();
                }
                if(scanned==5){
                    if(!lm.isEmpty()){
                        System.out.println("+---------------------+");
                        System.out.println("Please insert the person's name you want to deelete.");
                        System.out.println("+---------------------+");
                        String del = getsScan(scan);
                        if(lm.existinList(del)){
                            lm.RemoveElement(del);
                            System.out.println("+---------------------+");
                            System.out.println(del+" has been deleted successfully...");
                            System.out.println("+---------------------+");
                        } else {
                            System.out.println("+---------------------+");
                            System.out.println("This user doesn't exits in the array.");
                            System.out.println("+---------------------+");
                            //LinkedFunc(scan);
                        }
                    } else{
                        System.out.println("+---------------------+");
                        System.out.println("Please initialize the List first.");
                        System.out.println("+---------------------+");
                    }
                }
                if(scanned==6){
                    if(!lm.isEmpty()){
                        System.out.println("+---------------------+");
                            System.out.println("How many persons you want to register?");
                            System.out.println("+---------------------+");
                        int tp=getScan(scan, -999999, 999999);
                        for(int i=0;i<tp;i++){
                            System.out.println("Please insert the name.");
                            String name = getsScan(scan);
                            System.out.println("Please insert the surname.");
                            String surname = getsScan(scan);
                            System.out.println("Please insert the age.");
                            int age = getScan(scan, 0, 150);
                            System.out.println("Please insert the total of bank accounts.");
                            int totalbankaccounts = getScan(scan, 0, 200);
                            int[] banks = new int[totalbankaccounts];
                            for(int j=0;j<totalbankaccounts;j++){
                                System.out.println("Insert bank account nr. "+(j+1)+" of "+totalbankaccounts);
                                int bnt=getScan(scan, -99, 999999);
                                banks[j]=bnt;
                            }
                            lm.insertEnd(name, surname, age, banks);
                            System.out.println("+---------------------+");
                            System.out.println(name+" "+surname+" has been added successfully.");
                            System.out.println("+---------------------+");
                        }
                        System.out.println("+------------------------------+");
                    } else{
                        System.out.println("+---------------------+");
                        System.out.println("Please initialize the List first.");
                        System.out.println("+---------------------+");
                    }
                }
                if(scanned==7){
                    if(!lm.isEmpty()){
                        System.out.println("+---------------------+");
                        System.out.println("Please insert bank account name.");
                        String name=getsScan(scan);
                        System.out.println("Please insert bank account surname.");
                        String surname=getsScan(scan);
                        System.out.println("Please insert bank account number.");
                        int bankno=getScan(scan, 0,15000);
                        System.out.println("Please insert the new value.");
                        int val=getScan(scan, -9999, 9999999);

                        lm.ChangeBA(val, name, surname, bankno);
                        System.out.println("+---------------------+");
                    } else{
                        System.out.println("+---------------------+");
                        System.out.println("Please initialize the List first.");
                        System.out.println("+---------------------+");
                    }
                }
                if(scanned==8){
                    if(!lm.isEmpty()){
                        System.out.println("+---------------------+");
                        lm.displayList();
                        System.out.println("+---------------------+");
                        System.out.println("");
                    } else{
                        System.out.println("+---------------------+");
                        System.out.println("Please initialize the List first.");
                        System.out.println("+---------------------+");
                    }
                }
            }
    }
    
    static void StackFunc(Scanner scan){
        System.out.println("+--------------------------------+");
        System.out.println("1. Enter Elements in the stack.");
        System.out.println("2. Remove elements from the stack.");
        System.out.println("3. Print the name and total of bank accounts for top element.");
        System.out.println("4. Create a copy of the stack.");
        System.out.println("5. Show stack.");
        System.out.println("0. Go Back");
        System.out.println("+--------------------------------+");
        System.out.println("+--------------------------------+");
        System.out.println("Choose the size of the stack.");
        System.out.println("+--------------------------------+");
        int elements=getScan(scan, 0, 999999999);
        Stack st=new Stack(elements);
        System.out.println("Stack has been created with a maximum of "+elements+" elements.");
        System.out.println("+--------------------------------+");
        while(true){
            int scanned = getScan(scan, 0, 5);
            if(scanned==0){
                Start();
            }
            
            if(scanned==1){
                System.out.println("+--------------------------------+");
                System.out.println("How many elements do you want to add in the stack?");
                System.out.println("+--------------------------------+");
                int els=getScan(scan, 0, 999999999);
                for(int i=0;i<els;i++){
                    System.out.println("+--------------------------------+");
                    System.out.println("Please enter the name.");
                    String name = getsScan(scan);
                    System.out.println("Please enter surname.");
                    String surname = getsScan(scan);
                    System.out.println("Please enter age.");
                    int age = getScan(scan, 0, 999999999);
                    System.out.println("Please enter the number of bank accounts.");
                    int ba = getScan(scan, 0, 9999999);
                    st.push(name, surname, age, ba);
                }
                System.out.println("+--------------------------------+");
            }
            if(scanned==2){
                if(st.isEmpty()){
                    System.out.println("+--------------------------------+");
                    System.out.println("Stack is empty, nothing to delete.");
                    System.out.println("+--------------------------------+");
                } else {
                    st.pop();
                    System.out.println("+--------------------------------+");
                    System.out.println("Element deleted successfully");
                    System.out.println("+--------------------------------+");
                }
            }
            if(scanned==3){
                if(st.isEmpty()){
                    System.out.println("+--------------------------------+");
                    System.out.println("Stack is empty, nothing to show.");
                    System.out.println("+--------------------------------+");
                } else {
                    st.topElement();
                }
            }
            if(scanned==4){
                if(st.isEmpty()){
                    System.out.println("+--------------------------------+");
                    System.out.println("Stack is empty, nothing to copy.");
                    System.out.println("+--------------------------------+");
                } else {
                    Stack tm = st.copyStack(st);
                    System.out.println("+--------------------------------+");
                    System.out.println("The new stack is now copied as \"tm\"");
                    System.out.println("+--------------------------------+");
                }
            }
            if(scanned==5){
                if(st.isEmpty()){
                    System.out.println("+--------------------------------+");
                    System.out.println("Stack is empty, nothing to show.");
                    System.out.println("+--------------------------------+");
                } else {
                    st.showStack();
                }
            }
        }
    }
    
    static void QueueFunc(Scanner scan){
        System.out.println("+--------------------------------+");
        System.out.println("1. Enter Elements in the Queue.");
        System.out.println("2. Remove elements from the Queue.");
        System.out.println("3. Print the name and total of bank accounts for top element.");
        System.out.println("4. Create a copy of the Queue.");
        System.out.println("5. Show Queue.");
        System.out.println("0. Go Back");
        System.out.println("+--------------------------------+");
        System.out.println("+--------------------------------+");
        System.out.println("Choose the size of the Queue.");
        System.out.println("+--------------------------------+");
        int elements=getScan(scan, 0, 999999999);
        Queue qu=new Queue(elements);
        System.out.println("Queue has been created with a maximum of "+elements+" elements.");
        System.out.println("+--------------------------------+");
        while(true){
            int scanned = getScan(scan, 0, 5);
            if(scanned==0){
                Start();
            }
            if(scanned==1){
                System.out.println("+--------------------------------+");
                System.out.println("How many elements do you want to add in the Queue?");
                System.out.println("+--------------------------------+");
                int els=getScan(scan, 0, 999999999);
                for(int i=0;i<els;i++){
                    System.out.println("+--------------------------------+");
                    System.out.println("Please enter the name.");
                    String name = getsScan(scan);
                    System.out.println("Please enter surname.");
                    String surname = getsScan(scan);
                    System.out.println("Please enter age.");
                    int age = getScan(scan, 0, 999999999);
                    System.out.println("Please enter the number of bank accounts.");
                    int ba = getScan(scan, 0, 9999999);
                    qu.insert(name, surname, age, ba);
                }
                System.out.println("+--------------------------------+");
            }
            if(scanned==2){
                if(qu.isEmpty()){
                    System.out.println("+--------------------------------+");
                    System.out.println("Queue is empty, nothing to delete.");
                    System.out.println("+--------------------------------+");
                } else {
                    qu.remove();
                    System.out.println("+--------------------------------+");
                    System.out.println("Element deleted successfully");
                    System.out.println("+--------------------------------+");
                }
            }
            if(scanned==3){
                if(qu.isEmpty()){
                    System.out.println("+--------------------------------+");
                    System.out.println("Queue is empty, nothing to show.");
                    System.out.println("+--------------------------------+");
                } else {
                    qu.topElement();
                }
            }
            if(scanned==4){
                if(qu.isEmpty()){
                    System.out.println("+--------------------------------+");
                    System.out.println("Queue is empty, nothing to copy.");
                    System.out.println("+--------------------------------+");
                } else {
                    Queue qu1 = qu;
                    System.out.println("+--------------------------------+");
                    System.out.println("The new queue is now copied as \"qu1\"");
                    System.out.println("+--------------------------------+");
                }
            }
            if(scanned==5){
                if(qu.isEmpty()){
                    System.out.println("+--------------------------------+");
                    System.out.println("Queue is empty, nothing to show.");
                    System.out.println("+--------------------------------+");
                } else {
                    qu.showQueue();
                }
            }
        }
    }
    
    static void StructFunc(Scanner scan){
        System.out.println("+--------------------------------+");
        System.out.println("1. Register users.");
        System.out.println("2. Sort the array by age.");
        System.out.println("3. Find the maximum by total of the bank accounts.");
        System.out.println("4. Find the youngest.");
        System.out.println("5. Remove an element by name.");
        System.out.println("6. Add a new element.");
        System.out.println("7. Modify a bank account.");
        System.out.println("8. Display the array.");
        System.out.println("0. Go Back");
        System.out.println("+--------------------------------+");
        System.out.println("+--------------------------------+");
        System.out.println("Please set the maximum size of the Array...");
        System.out.println("+--------------------------------+");
        int arrSize=getScan(scan, 0, 9999999);
        Structure st = new Structure(arrSize);
        System.out.println("+--------------------------------+");
        System.out.println("Array size has been set to: "+arrSize);
        System.out.println("+--------------------------------+");
        
        while(true){
            int scanned = getScan(scan, 0, 8);
            if(scanned==0){
                Start();
            }
            if(scanned==1 || scanned==6){
                System.out.println("+--------------------------------+");
                System.out.println("How many customers do you want to register?");
                System.out.println("+--------------------------------+");
                int user=getScan(scan, 0, 90000000);
                if(user>st.getSize()){
                    System.out.println("Fatal EROR!!!........ Size is lower than the users you want to register.");
                    System.exit(0);
                }
                for(int i=0;i<user;i++){
                    System.out.println("+--------------------------------+");
                    System.out.println("Please urite the name of user "+(i+1)+" of "+user);
                    String name=getsScan(scan);
                    System.out.println("Please urite the surname of user "+(i+1)+" of "+user); 
                    String surname=getsScan(scan);
                    System.out.println("Please urite the age of user "+(i+1)+" of "+user);
                    int age=getScan(scan, 0, 500);
                    System.out.println("Please urite the total of bank accounts of user "+(i+1)+" of "+user);
                    int tb=getScan(scan, 0, 9999999);
                    int[] accounts= new int[tb];
                    for(int n=0;n<tb;n++){
                        System.out.println("Please write the bank account of user "+(n+1)+" of "+tb);
                        accounts[n]=getScan(scan, 0, 9999999);
                    }
                    st.addCustomer(name, surname, age, accounts);
                }
                System.out.println("+--------------------------------+");
            }
            if(scanned==2){
                System.out.println("+--------------------------------+");
                if(st.isEmpty()){
                    System.out.println("Nothing to sort, Array is empty");
                } else {
                    st.sort();
                    System.out.println("Array sorted successfully.");
                }
                System.out.println("+--------------------------------+");
            }
            if(scanned==3){
                System.out.println("+--------------------------------+");
                if(st.isEmpty()){
                    System.out.println("Cannot find anything, Array is empty");
                } else {
                    st.FindBAMax();
                }
                System.out.println("+--------------------------------+");
            }
            if(scanned==4){
                System.out.println("+--------------------------------+");
                if(st.isEmpty()){
                    System.out.println("Cannot find anything, Array is empty");
                } else {
                    st.FindYoungest();
                }
                System.out.println("+--------------------------------+");
            }
            if(scanned==5){
                System.out.println("+--------------------------------+");
                if(st.isEmpty()){
                    System.out.println("Cannot remove anything, Array is empty.");
                } else {
                    System.out.println("Please insert the name of user you want to remove...");
                    String name = getsScan(scan);
                    st.Remove(name);
                }
                System.out.println("+--------------------------------+");
            }
            if(scanned==7){
                System.out.println("+--------------------------------+");
                if(st.isEmpty()){
                    System.out.println("Cannot modify anything, Array is empty.");
                } else {
                    System.out.println("Please insert the name and surname of the account that you want to modify...");
                    String user = getsScan(scan);
                    if(st.ExistsInArray(user) > -1){
                        System.out.println("Pleas insert new name.");
                        String newname=getsScan(scan);
                        System.out.println("Please insert new surname.");
                        String newsurname=getsScan(scan);
                        System.out.println("Please inser new age.");
                        int newage=getScan(scan, 0, 250);
                        System.out.println("Please inser the new amount of bank accounts");
                        int newamount=getScan(scan, 0, 9999999);
                        int newamounts[]=new int[newamount];
                        for(int i=0;i<newamount;i++){
                            System.out.println("Please insert the value of Bank Account "+(i+1)+" of "+newamount);
                            newamounts[i]=getScan(scan, 0, 999999999);
                        }
                        st.ModifyAcc(newname, newsurname, newage, newamounts, st.ExistsInArray(user));
                    }
                    
                }
                System.out.println("+--------------------------------+");
            }
            if(scanned==8){
                System.out.println("+--------------------------------+");
                if(st.isEmpty()){
                    System.out.println("Nothing to show, Array is empty");
                } else {
                    st.display();
                }
                System.out.println("+--------------------------------+");
            }
        }
    }
    
    static void BinTreeFunc(Scanner scan){
        System.out.println("+--------------------------------+");
        System.out.println("1. Create a binary search tree based on the age by getting elements from the stack.");
        System.out.println("2. Create a binary search tree based on the total of bank accounts by getting the elemtents from the queue.");
        System.out.println("3. Find the total sum of bank accounts.");
        System.out.println("4. Find the average amount for each person.");
        System.out.println("5. Enter elements in the Tree.");
        System.out.println("6. Remove elements from the tree");
        System.out.println("0. Go back.");
        BinaryTree stree = new BinaryTree();
        BinaryTree qtree = new BinaryTree();
        Node nd=null;
        System.out.println("+--------------------------------+");
        System.out.println("Choose the size of the stack.");
        System.out.println("+--------------------------------+");
        int selements=getScan(scan, 0, 999999999);
        Stack st=new Stack(selements);
        System.out.println("Stack has been created with a maximum of "+selements+" elements.");
        System.out.println("+--------------------------------+");
        System.out.println("Choose the size of the Queue.");
        System.out.println("+--------------------------------+");
        int qelements=getScan(scan, 0, 999999999);
        Queue qu=new Queue(qelements);
        System.out.println("Queue has been created with a maximum of "+qelements+" elements.");
        System.out.println("+--------------------------------+");
        while(true){
            int scanned = getScan(scan, 0, 6);
            if(scanned==0){
                Start();
            }
            if(scanned==1){
                System.out.println("+--------------------------------+");
                System.out.println("How many elements do you want to add in the stack?");
                System.out.println("+--------------------------------+");
                int els=getScan(scan, 0, 999999999);
                for(int i=0;i<els;i++){
                    System.out.println("+--------------------------------+");
                    System.out.println("Please enter the name.");
                    String name = getsScan(scan);
                    System.out.println("Please enter surname.");
                    String surname = getsScan(scan);
                    System.out.println("Please enter age.");
                    int age = getScan(scan, 0, 999999999);
                    System.out.println("Please enter the number of bank accounts.");
                    int ba = getScan(scan, 0, 9999999);
                    st.push(name, surname, age, ba);
                    nd=new Node(st.peekBA(), "");
                    stree.InsertStack(st);
                    stree.addNode(st.peekBA(), "");
                }
                System.out.println("+--------------------------------+");
            }
            if(scanned==2){
                System.out.println("+--------------------------------+");
                System.out.println("How many elements do you want to add in the Queue?");
                System.out.println("+--------------------------------+");
                int els=getScan(scan, 0, 999999999);
                for(int i=0;i<els;i++){
                    System.out.println("+--------------------------------+");
                    System.out.println("Please enter the name.");
                    String name = getsScan(scan);
                    System.out.println("Please enter surname.");
                    String surname = getsScan(scan);
                    System.out.println("Please enter age.");
                    int age = getScan(scan, 0, 999999999);
                    System.out.println("Please enter the number of bank accounts.");
                    int ba = getScan(scan, 0, 9999999);
                    qu.insert(name, surname, age, ba);
                    qtree.InsertQueue(qu);
                    qtree.addNode(qu.peekBA(), "");
                }
                System.out.println("+--------------------------------+");
            }
            if(scanned==3){
                System.out.println("+--------------------------------+");
                System.out.println("Total sum of bank accounts is: "+stree.sum(nd));
                
            }
            if(scanned==5){
                System.out.println("+--------------------------------+");
                System.out.println("Please write the number you want to add in the tree");
                int num = getScan(scan, -999999, 99999999);
                stree.addNode(num, "");
                System.out.println("Value "+num+" has been added into the tree.");
                System.out.println("+--------------------------------+");
            }
            if(scanned==6){
                System.out.println("+--------------------------------+");
                System.out.println("Please write the number you want to remove from the tree.");
                int num = getScan(scan, -999999, 99999999);
                if(stree.isEmpty()){
                    System.out.println("Tree is empty.");
                } else {
                    stree.remove(num);
                    System.out.println("Value "+num+" has been removed from the tree.");
                }
                System.out.println("+--------------------------------+");
            }
        }
    }
    
    static int getScan(Scanner scan,int val1, int val2){
            try{
            int num = scan.nextInt();
            if(num>val2 || num<val1){
                System.out.println("+--------------------------------+");
                System.out.println("!!!.........System Failure!...........!!!");
                System.out.println("Plase make sure you enter a number from "+val1+" to "+ val2);
                System.exit(0);
            }
            return num;
        } catch(Exception e){
                System.out.println("!!!.........System Failure!...........!!!");
                System.exit(0);
        }
            return 0;
    }
    
    static String getsScan(Scanner scan){
        return scan.next();
    }
}
