package pkgfinal;
public class Queue {
    private String names[];
    private String surnames[];
    private int ages[];
    private int bankAccounts[];
    private int queueSize;
    private int front, numberOfItems, rear = 0;
   
    public Queue(int num){
        queueSize=num;
        names = new String[queueSize];
        surnames = new String[queueSize];
        ages = new int[queueSize];
        bankAccounts = new int[queueSize];
        

    }
    public boolean isEmpty(){
        return numberOfItems==0;
    }
    public void insert(String name, String surname, int age, int bankaccount){
        if(numberOfItems + 1 <= queueSize){
            names[rear] = name;
            surnames[rear] = surname;
            ages[rear] = age;
            bankAccounts[rear] = bankaccount;
            rear++;
            numberOfItems++;
        } else {
            System.out.println("+--------------------------------+");
            System.out.println("Sorry, But the Queue is Full");
            System.out.println("+--------------------------------+");
        }
    }
   
    public void remove(){
        if(numberOfItems > 0){
            names[front] = "";
            surnames[front]="";
            ages[front]=-1;
            bankAccounts[front]=-1;
            front++;
            numberOfItems--;
        } else {
            System.out.println("+--------------------------------+");
            System.out.println("Sorry, But the Queue is Empty");
            System.out.println("+--------------------------------+");
        }
    }
    
    public int peekAge(){
        if(isEmpty()){
            return -999999;
        }
        return ages[front];
    }
    public int peekBA(){
        if(isEmpty()){
            return -999999;
        }
        return bankAccounts[front];
    }
    
    public void showQueue(){
        System.out.println("+--------------------------------+");
        for(int i=front;i<rear;i++){
                System.out.println("Name: "+names[i]);
                System.out.println("Surname: "+surnames[i]);
                System.out.println("Age: "+ages[i]);
                System.out.println("Bank Accounts: "+bankAccounts[i]);
                System.out.println("+--------------------------------+");
        }
    }
   
    public int size(){
        return queueSize;
    }
    
    public void topElement(){
        System.out.println("+--------------------------------+");
        System.out.println("Top element is: "+names[size()-1]+" and he/she has "+bankAccounts[size()-1]+" bank accounts.");
        System.out.println("+--------------------------------+");
    }
    
}
