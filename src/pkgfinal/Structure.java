package pkgfinal;
public class Structure {
    private Customer[] bank;
    private int total=0;
    public Structure(int val){
        bank= new Customer[val];
    }
    
    public int getSize(){
        return bank.length;
    }
    public boolean isFull(){
        return total==getSize();
    }
    public boolean isEmpty(){
        return total==0;
    }
    
    public void addCustomer(String name, String surname, int age,int [] bk){
        if(!isFull()){
            Customer newcustomer = new Customer(name,surname,age,bk);
            bank[total]=newcustomer;
            total++;
        }
    }
    public void sort() {
        if(!isEmpty()){
            Customer tmp;
            for (int i=0;i<total;i++) {
                for (int j=i+1;j<total;j++) {
                    if (bank[j].age<bank[i].age) {
                        tmp =bank[i];
                        bank[i] = bank[j];
                        bank[j] = tmp;
                    }
                }
            }
        }
    } 
    public void FindBAMax(){
        if(isEmpty()){
            return;
        }
        String name=bank[0].name;
        String surname=bank[0].surname;
        int age=bank[0].age;
        int banks=bank[0].bk.length;
        for(int i=0;i<total;i++){
            if(bank[i].bk.length>banks){
                name=bank[i].name;
                surname=bank[i].surname;
                age=bank[i].age;
                banks=bank[i].bk.length;
            }
        }
        System.out.println("Customer who has most of Bank accounts is:");
        System.out.println(name+" "+surname+",he/she is: "+age+" years old, and has "+banks+" bank accounts.");
    }
    public void display(){
        if(!isEmpty()){
            for(int i=0;i<total;i++){
                System.out.println("Name: "+bank[i].name);
                System.out.println("Surname: "+bank[i].surname);
                System.out.println("Age: "+bank[i].age);
                for(int j=0;j<bank[i].bk.length;j++){
                    System.out.println("Bank Account "+(j+1)+": "+bank[i].bk[j]);
                }
                System.out.println("+--------------------------------+");
            }
        }  
    }
    public int ExistsInArray(String val){
        if(isEmpty()){
            return -1;
        }
        for(int i=0;i<total;i++){
            String user = bank[i].name+bank[i].surname;
            if(user.toLowerCase().equals(val.toLowerCase())){
                return i;
            }
        }
        return -1;
    }
    public void FindYoungest(){
        if(isEmpty()){
            return;
        }
        String name=bank[0].name;
        String surname=bank[0].surname;
        int age=bank[0].age;
        int banks=bank[0].bk.length;
        for(int i=1;i<total;i++){
            if(bank[i].age < age){
                name=bank[i].name;
                surname=bank[i].surname;
                age=bank[i].age;
                banks=bank[i].bk.length;
            }
        }
        System.out.println("Youngest is:");
        System.out.println(name+" "+surname+",he/she is: "+age+" years old, and has "+banks+" bank accounts.");
    }
    public void Remove(String name){
        if(isEmpty()){
            return;
        }
        boolean removed=false;
        for(int i=0;i<total;i++){
            if(bank[i].name.toLowerCase().equals(name.toLowerCase())){
                for(int j=i;j<total-1;j++){
                    bank[j]=bank[j+1];
                }
                total=total-1;
                removed=true;
                break;
            }
        }
        if(removed){
            System.out.println("User "+name+" has been removed successfully from the array.");
        } else {
            System.out.println("I'm sorry, but this user "+name+" is not found in this array.");
        }
    }
    public void ModifyAcc(String name, String surname, int age, int bk[], int i){
        if(isEmpty()) return;
        bank[i].name=name;
        bank[i].age=age;
        bank[i].surname=surname;
        bank[i].bk=bk;
    }
}
class Customer {
    public String name;
    public String surname;
    public int age;
    public int [] bk;
    

    Customer(String Name, String Surname, int Age,int[] bankaccount) {
        this.name=Name;
        this.surname=Surname;
        this.age=Age;
        this.bk=bankaccount;
    }
    
    public String getName(){
        return this.name;
    }
    public String getSurname(){
        return this.surname;
    }
    public int getAge(){
        return this.age;
    }
    public int[] getBK(){
        return this.bk;
    }
    public void setName(String val){
        this.name=val;
    }
    public void setSurname(String val){
        this.surname=val;
    }
    public void setAge(int val){
        this.age=val;
    }
    public void setBK(int[] val){
        this.bk=val;
    }
}