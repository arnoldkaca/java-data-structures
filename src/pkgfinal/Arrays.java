package pkgfinal;
public class Arrays{
    public static int array[];
    public static boolean arrayset=false;
    //Exercise 1-b
    public static void SortArray(int arr[]){
        for(int i=0;i<arr.length;i++){
            for(int j=i;j<arr.length;j++){
                if(arr[i]>arr[j]){
                    int temp= arr[i];
                    arr[i] = arr[j];
                    arr[j] = temp;
                }
            }
        }
    }
    //Exercise 1-c
    public static int FindMax(int arr[]){
        int max = arr[0];
        for(int i=0;i<arr.length;i++){
            if(arr[i]>max){
                max=arr[i];
            }
        }
        return max;
    }
    //Exercise 1-d
    public static int FindMin(int arr[]){
        int min = arr[0];
        for(int i=0;i<arr.length;i++){
            if(arr[i]<min){
                min=arr[i];
            }
        }
        return min;
    }
    //Exercise 1-e - >The most entered value(mode)
    public static int FindMode(int arr[]){
        int mode=arr[0];
        int count = 0;
        int tmp_mode=arr[0];
        int tmp_count = 0;
        for(int i=0;i<arr.length;i++){
            if(arr[i]==mode){
                count=count+1;
            }
            for(int j=0;j<arr.length;j++){
                tmp_mode=arr[i];
                if(tmp_mode==arr[j]){
                    tmp_count = tmp_count+1;
                }
            }
            if(count<tmp_count){
                mode=tmp_mode;
            }
            tmp_count=0;
        }
        return mode;
    }
    //Exercise 1-f -> Find Equillibrium
    public static boolean HasEquillibriumElement(int arr[]){
        int left_sum=0;
        int right_sum=0;
        if(arr.length % 2 == 0) return false;
        for(int i=0;i<(arr.length-1)/2;i++) left_sum=left_sum+arr[i];
        for(int i=(arr.length+1)/2;i<arr.length;i++) right_sum=right_sum+arr[i];
        return left_sum == right_sum;
    }
    public static int[] Median(int arr[]){
        int[] median = new int[2];
        if(HasEquillibriumElement(arr)){
            median[0]=(arr.length-1)/2;
            median[1]=arr[(arr.length-1)/2];
        }
        return median;
    }
    //Exercise 1-g -> Remove an Element
    public static boolean ExistsinArray(int arr[], int ex){
        for(int i=0;i<arr.length;i++) if(arr[i]==ex) return true;
        return false;
    }
    public static void RemoveElement(int arr[], int val){
        if(!ExistsinArray(arr, val)){
            System.out.println("Element doesn't exists in the array.");
            System.out.println("+-------------------------+");
            return;
        }
        for(int i=0;i<arr.length-1;i++){
            if(arr[i]==val){
                val=-9999;
                arr[i]=val;
                int tmp=arr[i];
                arr[i]=arr[i+1];
                arr[i+1]=tmp;
            }
        }
        
        
        
    }
    //Exercise 1-h -> Add an element
    public static void AddElement(int arr[], int pos, int val){
        for(int i=0;i<arr.length;i++){
            if(i==pos){
                for(int j=i;j<arr.length;j++){
                    int tmp=arr[i];
                    arr[i]=arr[j];
                    arr[j]=tmp;
                }
                arr[pos]=val;
                break;
            }
        }
        
    }
    public static void ShowArray(int arr[]){
        for(int i=0;i<arr.length;i++){
            if(i==arr.length-1){
                System.out.print(arr[i]+ ".");
            }else{
                System.out.print(arr[i]+ ", ");
            }
        }
        System.out.println("");
    }
    
    
    public static void setArraySize(int num){
        array = new int[num];
    }
}
